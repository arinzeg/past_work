# README #

A small collection of code from past projects

- Leanmapreduce folder holds code files used in demonstrating proof-of-concept of my thesis
- CI\_project\_2015 contains files, including a report, for a smaller project which tends towards continuous integration
- openVswitch contains files for building a network around virtual switches which facilitate proper traffic control
