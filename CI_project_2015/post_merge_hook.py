#!/usr/bin/env python

import paramiko
import sys
import subprocess

hostname = '10.0.30.27'
port = 22
username = 'ubuntu'
ssh_key = '/home/ubuntu/.ssh/projec_test'
sys.stdin = open('/dev/tty')

host_name = raw_input("please enter webserver's name >>> ")
host_name = host_name.strip()

search_word = raw_input("please enter the search word >>> ")
search_word = search_word.strip()Project1

version = raw_input("please enter the version number, e.g v2 >>> ")
version = version.strip()

if __name__ == "__main__":
	key = paramiko.RSAKey.from_private_key_file(ssh_key)
	s = paramiko.SSHClient()
	s.load_system_host_keys()
	s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	s.connect(hostname, port, pkey=key)
	stdin, stdout, stderr = s.exec_command('cd /home/ubuntu ; ./build_server.py %s %s' % (host_name,search_word))
	out = stdout.read()
	output = open("exec_output.txt", "w")
	output.write(out)
	output.close()
	s.close()
if "successful test" in out:
	print "thanks for your patience muchachos"
	subprocess.call("cd /home/ubuntu/racoon_inc ; git tag -a %s -m ’my version %s’ ; git push origin %s " % (version,version,version), shell=True)
elif "test failed" in out:
	print "Test failed; please check the code...rolling back to pre-merge state..."
	subprocess.call("cd /home/ubuntu/racoon_inc ; git reset --hard HEAD ̃1 ; git checkout development", shell=True)
	print "Current branch: development"
else:
	print "there are missing tags in your code or the connection to the website failed"
