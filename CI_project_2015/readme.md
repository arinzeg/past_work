**build\_server** is responsible for building virtual machines purposed for testing the latest changes when an attempt is made to merge with the main branch prior to a push into the production server.
**post\_merge\_hook** is a custom local git hook which triggers the aforementioned code test.

More about the end-to-end process can be found in **CI\_report**