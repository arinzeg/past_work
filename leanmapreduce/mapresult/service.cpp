// This file is a part of the IncludeOS unikernel - www.includeos.org
//
// Copyright 2015-2016 Oslo and Akershus University College of Applied Sciences
// and Alfred Bratterud
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*

 * Sends the results to a server
 * 

*/

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <map>
#include <algorithm>
#include <os>
#include <net/inet4>


using Connection_ptr = std::shared_ptr<net::TCP::Connection>;

// An IP-stack object
std::unique_ptr<net::Inet4<VirtioNet> > inet;

// IP of server to send the result to
net::TCP::Socket python_server( {{10,0,0,1}} , 1337);


using namespace std;
static inline std::string &rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        return s;
}

void Service::start() {
  // Assign a driver (VirtioNet) to a network interface (eth0)
  hw::Nic<VirtioNet>& eth0 = hw::Dev::eth<0,VirtioNet>();
  
  // Bring up a network stack, attached to the nic
  inet = std::make_unique<net::Inet4<VirtioNet> >(eth0);
  
  // Static IP configuration, until we (possibly) get DHCP
  inet->network_config( {{ 10,0,0,42 }},      // IP
			{{ 255,255,255,0 }},  // Netmask
			{{ 10,0,0,1 }},       // Gateway
			{{ 8,8,8,8 }} );      // DNS

   // to the computation

	map<string, size_t> word_count;
	string text = "bigfile";
	
	istringstream single(text);
	while (single)
	{
		string word;
		single >> word;
		for (auto &x : word)
		{
			x = tolower(x);
			if (!isalpha(x))
			{
				x=' ';
			}
		}
		word = rtrim(word);
		++word_count[word];
	}
//	myfile.close();
	text.clear();
	string json="";
	string open_curl="{";
	string close_curl="}";
	json += open_curl.c_str();
	for (const auto &w : word_count)
	{
		if (w.first.size()== 0)
		{
			continue;
		}
		string first = w.first;
		
		json += first;
		//json.erase(json.find_first_of(' '));
		json += ':';
		string second = to_string(w.second);
		json += second;
		json += '*';
		
		}

	json.erase(json.find_last_of("*"));
	json += close_curl.c_str();
	//printf(json.c_str());
	

   printf("Connecting to the server");
   // Make an outgoing connection to our python server

   auto outgoing = inet->tcp().connect(python_server);
   // When outgoing connection to python sever is established
   outgoing->onConnect([json](Connection_ptr python) {
      printf("Connected [Python]: %s\n", python->to_string().c_str());
		   
		        
      // When client is disconnecting
      python->onDisconnect([python](Connection_ptr, std::string msg) {
	 printf("Disconnected from the server");
	 python->close();
      });
      
      printf("Wrinting result\n");
      python->write( json.data(), json.size() );
      
      printf("closing connection\n");
      python->close();
      

   }); // << onConnect (outgoing (python))
}


