# README #

This project dynamically rolls out a MapReduce cluster following a strategy inspired by the horizontal (altabackup folder) and vertical growth abstractions of a B-Tree (altbbackup folder). This is in accordance with the strategies and designs laid out in my master's thesis work, Lean MapReduce: A B-tree Inspired Mapreduce Framework.
It is therefore relevant that the readers familiarize themselves with the substance of the thesis work.

The map tasks are performed by a simple word-counting procedure written in C++. This is contained in the service.cpp file, which is located inside the mapresult folder. It is converted to a virtual machine disk image when compiled in combination with the complete binary of IncludeOS unikernel OS

-----------------------------------------------------------------------------------------------------------------------

Leanmapreduce requires Ubuntu 14.04 Trusty, an openstack cloud installation, and Manage Large Network (MLN) which provides a simple format for templating virtual machines and a set of commands for autonomating the creation of networks of scale




