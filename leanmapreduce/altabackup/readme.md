This code base implements a strategy inspired by the vertical growth component of a B-tree data structure (when aligned with the customary coordinate axes) in rolling out a MapReduce infrastructure

* maprunaltAmain.py, written in Python initiates the installation of the mapreduce infrastructure and the data processing cycle. It pipelines data through the phasal functions of map and reduce, and writes the final result to disk. Kindly read the thesis report for a finer breakdown.
* Ossim.sh, written in BASH, is a trivial simulator which mimicks the behavior of an IncludeOS-bound map function in terms of the expected result. This aids a fast-tracked demonstration of relative time performance of the rolloout technique
* Generator.py, written in Python, is a simple approximate-size text file generator (big data). E.g 1026MB, 2052MB, etc.
* Server3\_main.py, written in Python, comprises two distinct components: a socket server (inherits Python asynchore.dispatcher and asynchore.dispatcher\_with\_send classes) which receives results asynchronously over http from map tasks; and a reduce class which merges all inbound key:value lists, by keys, from downstream components, and delivers a unit result to upstream targets
* Iterator.py automates one complete test cycle
* Spawnup is an artifact which is updated over multiple complete test cycles in one test session. It captures the amount of time from request submission to successful infrastructure installation and configuration, prior to data processing

The files in the **transfolder** are utilized by next-line (supervisors) hosts, as long as the down-stream chain goes, in extending the infrastructure in order to sufficiently, but not wastefully, accomodate the workload in scope