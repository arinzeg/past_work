#!/usr/bin/python

import sys
import subprocess
import os
import time
import paramiko
import random
import socket
import math
import re
import shlex


localdir = "/home/ubuntu/mapproject/"
transfolder = "/home/ubuntu/mapproject/transfolder/"
remotedir = "/home/ubuntu/mapproject/"
port = 22
pkey_file = "/home/ubuntu/.ssh/id_rsa"
serverip = sys.argv[2]
localip = sys.argv[1]


def pre_spawn(count):
	ips = 44
	for n in range(count+1):
		if n < 10:
                        if n == count:
                                time.sleep(5)
                                break
                        os.chdir(localdir)
                        subprocess.call(["cp", "-r", "mapresult", "mapresult00%d" % n])
                        subprocess.call(["mv", "bigfile00%d" % n, "mapresult00%d/" % n])
                        os.chdir("%s/mapresult00%d" % (localdir,n))
                        subprocess.call(["sed", "-i", "s/10,0,0,42/10,0,0,%d/g" % ips, "service.cpp"])
                        subprocess.call(["sed", "-i", "s/10.0.0.%d/10,0,0,%d/g" % (ips,ips), "service.cpp"])
                        subprocess.call(["sed", "-i", "s/bigfile/bigfile00%d/g" % n, "service.cpp"])
                        subprocess.call(["sed", "-i", "/^$/d", "bigfile00%d" % n])
                        subprocess.call(["sed", "-i", r"s/$/\\/g", "bigfile00%d" % n])
                        subprocess.call(["sed", "-i", r"$ s/\\$//g", "bigfile00%d" % n])
                        subprocess.call("truncate --size=-1 bigfile00%d" % n, shell=True)
                        subprocess.call(["perl", "-i", "-pe", "s/bigfile00%d/`cat bigfile00%d`/ge" % (n,n), "service.cpp"])
                        subprocess.call("rm bigfile00%d" % n, shell=True)
                        fnull = open(os.devnull, 'w')
                        run = subprocess.Popen(["make"], stdout=fnull, close_fds=True)
                        run.wait()
			ips += 1
                else:
                        if n == count:
                                time.sleep(5)
                                break
                        os.chdir(localdir)
                        subprocess.call(["cp", "-r", "mapresult", "mapresult0%d" % n])
                        subprocess.call(["mv", "bigfile0%d" % n, "mapresult0%d/" % n])
                        os.chdir("%s/mapresult0%d" % (localdir,n))
                        subprocess.call(["sed", "-i", "s/10,0,0,42/10,0,0,%d/g" % ips, "service.cpp"])
                        subprocess.call(["sed", "-i", "s/10.0.0.%d/10,0,0,%d/g" % (ips,ips), "service.cpp"])
                        subprocess.call(["sed", "-i", "s/bigfile/bigfile0%d/g" % n, "service.cpp"])
                        subprocess.call(["sed", "-i", "/^$/d", "bigfile0%d" % n])
                        subprocess.call(["sed", "-i", r"s/$/\\/g", "bigfile0%d" % n])
                        subprocess.call(["sed", "-i", r"$ s/\\$//g", "bigfile0%d" % n])
                        subprocess.call("truncate --size=-1 bigfile0%d" % n, shell=True)
                        subprocess.call(["perl", "-i", "-pe", "s/bigfile0%d/`cat bigfile0%d`/ge" % (n,n), "service.cpp"])
                        subprocess.call("rm bigfile0%d" % n, shell=True)
                        fnull = open(os.devnull, 'w')
                        run = subprocess.call(["make"], stdout=fnull, close_fds=True)
                        ips += 1

def start_env(count):
        pre_spawn(count)
	cpucount = 0
        ips = 44
        for m in range(count + 1):
                if m==count:
                        time.sleep(8)
                        break
                if m < 10:
                        os.chdir("%s/mapresult00%d" % (localdir,m))
                        mac = randomMAC()
                        args = "/usr/bin/qemu-system-x86_64 --enable-kvm -drive file=mapresult.img,format=raw,if=ide -device virtio-net,netdev=net0,mac=%s -netdev tap,id=net0,script=/root/IncludeOS_install/etc/qemu-ifup -name includeOS%d -vga none -nographic -smp 1 -m 200" % (mac,m)
                        args = shlex.split(args)

                        print "starting up vm at 10.0.0.%d" % ips
                        fnull=open(os.devnull, 'w')
                        subprocess.Popen(["taskset", "-c", "%d" % cpucount, "/bin/bash", "OSsim.sh", "%s" % localip], stdout=fnull, close_fds=True)
                        print "started up vm at 10.0.0.%d" % ips
                        cpucount += 1
                        if cpucount == 8:
                                cpucount = 0
                else:
                        os.chdir("%s/mapresult0%d" % (localdir,m))
                        mac = randomMAC()
                        args = "/usr/bin/qemu-system-x86_64 --enable-kvm -drive file=mapresult.img,format=raw,if=ide -device virtio-net,netdev=net0,mac=%s -netdev tap,id=net0,script=/root/IncludeOS_install/etc/qemu-ifup -name includeOS%d -vga none -nographic -smp 1 -m 200" % (mac,m)
                        args = shlex.split(args)

                        print "starting up vm at 10.0.0.%d" % ips
                        fnull=open(os.devnull, 'w')
                        subprocess.Popen(["taskset", "-c", "%d" % cpucount, "/bin/bash", "OSsim.sh", "%s" % localip], stdout=fnull, close_fds=True)
                        print "started up vm 10.0.0.%d" % ips
                        cpucount += 1
                        if cpucount == 8:
                                cpucount = 0
                ips += 1

def installMLN():
	os.chdir("/home/ubuntu")
        subprocess.check_call("apt-get install -y  git", shell=True)
        try:
                subprocess.check_call("git clone https://github.com/kybeg/mln.git", shell=True)
        except:
                pass
        try:
                subprocess.check_call("apt-get install -y python-novaclient", shell=True)
        except:
                pass
        os.chdir("/home/ubuntu/mln/")
        new = subprocess.Popen(["./mln" ,"setup"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        time.sleep(4)
        new.stdin.write("2\n")
        i = 0
        while i < 7:
                time.sleep(2)
                new.stdin.write("\n")
                i +=1

spawnend = None
spawnstart = None
def prepRemote(localip):
        os.chdir("/home/ubuntu/mln")
        copy = subprocess.call("cp /home/ubuntu/.ssh/id_rsa.pub /var/www/html/.", shell=True)
        try:
                subprocess.check_call("rm /home/ubuntu/.ssh/known_hosts", shell=True)
        except:
                print "no known_hosts file"
        subprocess.call(["cp", "leanmr.mln", "leanmrtemp4.mln"])
        subprocess.call(["sed", "-i", "s/ipaddress/%s/g" % localip, "leanmrtemp4.mln"])
        subprocess.call(["sed", "-i", "s/leanmr/leanmra4/g", "leanmrtemp4.mln"])
        subprocess.call(["sed", "-i", "s/downstream1/downstreama4/g", "leanmrtemp4.mln"])
        fnull = open(os.devnull, 'w')
	global spawnstart
	spawnstart = time.time()
        subprocess.call(["/usr/local/bin/mln", "build","-r","-f", "leanmrtemp4.mln"],stdout=fnull, stderr=subprocess.STDOUT, close_fds=True)
        subprocess.call(["/usr/local/bin/mln", "start", "-p", "leanmra4"], stdout=fnull, stderr=subprocess.STDOUT, close_fds=True)
        status = False
        ipaddress = None
        reg_pattern = re.compile(r"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b")
        while status==False:
                mln_stat = subprocess.Popen(["/usr/local/bin/mln", "status", "-p", "leanmra4", "-h", "downstreama4"], stdout=subprocess.PIPE)
                entry, error = mln_stat.communicate()
                ipaddress = reg_pattern.findall(entry)
                for i in entry.split():
                        if "downstreama4" in i and ipaddress:
                                ipaddress = ipaddress[0]
                                ipaddress = ipaddress.strip()
                                print "Address found! >> ", ipaddress
                                status = True
                        else:
                                continue
        subprocess.Popen(["rm","/home/ubuntu/mln/leanmrtemp4.mln"])
        return ipaddress


def ping(hostname):
        command = ["ping", "-c", "1", "-w", "2",  "%s" % hostname]
        runpng = subprocess.Popen(command, stdout = subprocess.PIPE)
        pngresult = runpng.communicate()[0]
        print pngresult
        pngresult = pngresult.split("\n")
        pngtime = pngresult[1].split()
        if "time" in pngtime[-2]:
                return True
        else:
                return False
                runpng.stdout.flush() 



def randomMAC():
        mac = [ 0xc0, 0x01, 0x0a,
                random.randint(0x00, 0x7f),
                random.randint(0x00, 0xff),
                random.randint(0x00, 0xff) ]
        return ':'.join(map(lambda x: "%02x" % x, mac))



os.chdir("/home/ubuntu/mapproject")
subprocess.Popen(["mkdir", "transfolder"])
subprocess.Popen("rsync -av --exclude='transfolder/' --exclude='bigfile*' /home/ubuntu/mapproject/ transfolder", shell=True)


max_data_per_vm = None
if not sys.argv[3]:
        #Prompt the user to do so if they have not fed in the data split limit
        max_data_per_vm = int(raw_data("please enter the max size of data for each VM: >>>   "))
else:
        max_data_per_vm = int(sys.argv[3]) * 1024 * 1024


stre = ['cat', '/proc/meminfo']
stre1 = ['awk', '/MemFree:/ { mem = $2 } /^Cached:/ { cached = $2 } END { print (mem + cached) * 1024 }']

prelim = subprocess.Popen(stre, stdout=subprocess.PIPE)
getsize = subprocess.Popen(stre1, stdin=prelim.stdout,stdout=subprocess.PIPE)
availmem = getsize.communicate()[0]
availmem = float(availmem)

to_alloc = int(math.ceil((availmem - (availmem *(9.32/10.0)))))

max_vm_mem_size = max_data_per_vm * 3
filesize = os.path.getsize("/home/ubuntu/mapproject/bigfile.txt")
filesize = int(math.ceil(float(filesize)))
numof_instances = None

#sentinel variable to trigger cluster growth
greater = False
#file counter of input splits to be fed to local mappers
filecnt = 0
#keep a global counter of excess input splits in case it is needed in future
excesscnt = 0

if to_alloc >= filesize:
        numof_instances = int(math.ceil(float(filesize) / max_data_per_vm))
        subprocess.check_call(["split","-d", "--number=%d" % numof_instances, "bigfile.txt", "bigfile0"])
else:
        #
        greater = True
        subprocess.check_call(["split","-d", "--line-bytes=%d" % to_alloc, "bigfile.txt", "tempbig0"])
        numof_instances = int(math.ceil(float(to_alloc)/max_data_per_vm))
        filecnt = 0
        for i in os.listdir("/home/ubuntu/mapproject"):
                if "tempbig" in i:
                        excesscnt += 1


        #excess data is combined into a new big data file for transmission to downstream supervisor
        xdata = os.path.join(transfolder, "bigfile.txt")
        os.chdir("/home/ubuntu/mapproject")
	for m in range(1,excesscnt):
        	if m < 10:
			subprocess.check_call("cat tempbig00%d >> %s" % (m,xdata), shell=True)
			subprocess.check_call(["rm", "tempbig00%d" % m])
		else:
			subprocess.check_call("cat tempbig0%d >> %s" % (m, xdata), shell=True)
			subprocess.check_call(["rm", "tempbig0%d" % m])
        subprocess.check_call(["split","-d", "--number=%d" % numof_instances, "tempbig000", "bigfile0"])

filecnt = 0
for i in os.listdir("/home/ubuntu/mapproject/"):
        if "bigfile0" in i:
                filecnt += 1

spawntime = None

if greater == True:
	installMLN()
	time.sleep(4)
	subprocess.check_call(["cp", "/home/ubuntu/mapproject/transfolder/leanmr.mln", "/home/ubuntu/mln/leanmr.mln"])
	subprocess.check_call(["cp", "/home/ubuntu/mapproject/transfolder/.openstack", "/home/ubuntu/.openstack"])
        hostip = prepRemote(localip)
        state = False
        while state != True:
                try:
                        state = ping(hostip)
                        print "Trying to reach %s " % hostip
                except:
                        print "the host is unreachable"
                        continue

        print "We have a go"
        #ensure all packages have been installed. Apache2 is meant to be the last so it is safe to pin the test to it
        check = False
        while check==False:
                try:
                        fnull = open(os.devnull, 'w')
                        curl = subprocess.check_call(["curl", "-s", "%s" % hostip], stdout=fnull, close_fds=True)
                        if curl == 0:
                                check=True
                        else:
                                check=False
                except:
                        check=False
                        print "not ready ..."


        command ="mkdir mapproject"
        other = "yes\n"
        username = "ubuntu"
        key = paramiko.RSAKey.from_private_key_file(pkey_file)
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.load_system_host_keys()
        ssh.connect(hostip, username=username, pkey=key)
        stdin, stdout, stderr = ssh.exec_command(command)
        try:
                stdin.write(other)
                time.sleep(2)
                stdin.flush()
        except:
                print "not expecting input but proceeding ..."
		time.sleep(4)
        try:
		config = "Host %s \nStrictHostKeyChecking no\nUserKnownHostsFile=/dev/null" % hostip
		files = open("/home/ubuntu/mapproject/transfolder/config", 'w')
		files.write(config)
		files.close()
	except:
		print "could not create file"
	try:
		subprocess.check_call(["mv", "/home/ubuntu/mapproject/transfolder/config", "/root/.ssh/"])
		
	except:
		print "Copy-config operation failed"
		
	try:
		scp = "scp -v -i /home/ubuntu/.ssh/id_rsa -r /home/ubuntu/mapproject/transfolder/. ubuntu@%s:/home/ubuntu/mapproject/." % hostip
		scp = shlex.split(scp)
		other ="yes\n"
		transfer = subprocess.Popen(scp, stdout=subprocess.PIPE, stdin=subprocess.PIPE)
		time.sleep(3)
		transfer.stdin.write(other)
		transfer.stdout.flush()

        except:
                print "authentication failed: Arinze, find out what the problem is"
        
        time.sleep(20)
        data_size = int(math.ceil(float(max_data_per_vm/(1024*1024))))
        command = "sudo screen -L -d -m python /home/ubuntu/mapproject/maprunaltA3.py %s %s %d" % (hostip, localip, data_size)
       
        stdin, stdout, stderr = ssh.exec_command(command)
        time.sleep(3)
        try:
                stdin.write(other)
                stdin.flush()
        except:
                print "not expecting input but proceeding..."
        time.sleep(3)
        ssh.close()
	spawnend = time.time()
	spawntime = spawnend - spawnstart


time.sleep(5)



os.chdir("/home/ubuntu/")
fnull = open(os.devnull, 'w')
subprocess.check_call(["git", "clone", "https://github.com/hioa-cs/IncludeOS.git"], stdout=fnull, close_fds=True)

os.chdir("/home/ubuntu/IncludeOS/src")
subprocess.check_call("/usr/bin/make clean", shell=True)
os.chdir("/home/ubuntu/IncludeOS/vmbuild")
subprocess.check_call("/usr/bin/make clean", shell=True)
os.chdir("/home/ubuntu/IncludeOS/etc")
subprocess.check_call("./install_from_bundle.sh", shell=True)
ramsize = int(math.ceil(float(max_vm_mem_size)/(1024*1024)))
subprocess.check_call("sed -i s/128/%d/g /home/ubuntu/IncludeOS_install/etc/qemu_cmd.sh" % ramsize, shell=True)


#start server socket
os.chdir("/home/ubuntu/mapproject/")
if greater ==True:
	subprocess.Popen(["python", "server3_ds.py", "%s" % str(filecnt+1)])
else:
	subprocess.Popen(["python", "server3_ds.py", "%s" % str(filecnt)])

start_env(filecnt)

final = False
while final == False:
	if os.path.isfile("/home/ubuntu/mapproject/finalresult.txt") == True:
		final = True
		if greater == True:
			
			scp = "scp -v -i /home/ubuntu/.ssh/id_rsa ubuntu@%s:/home/ubuntu/mapproject/spawnup /home/ubuntu/spawnup" % hostip
			scp = shlex.split(scp)
			other ="yes\n"
			transfer = subprocess.Popen(scp, stdout=subprocess.PIPE, stdin=subprocess.PIPE)
			time.sleep(5)
			try:
				transfer.stdin.write(other)
				transfer.stdout.flush()
			except:
				print "no need for a 'yes'"
			extra = open("/home/ubuntu/spawnup", "r")
			value = float(extra.read())
			extra.close()
			spawntime = value + spawntime
            spawnup = open("/home/ubuntu/mapproject/spawnup", 'a+')
			spawnup.write(str(spawntime) + "\n")
			spawnup.close()
			subprocess.check_call("/bin/bash /home/ubuntu/mapproject/iterator2.sh", shell=True)
		time.sleep(5)
else:
	time.sleep(5)
	s = socket.socket()
	host = serverip
	print "server ip >> ", host
	port = 1337
	s.connect((host,port))
	data = open("/home/ubuntu/mapproject/finalresult.txt", "r")
	l =data.read(1024)
	while l:
        	print "sending ............##########################  \n\n"
        	s.send(l)
        	l = data.read(1024)
	data.close()
	s.close()



	
