#!/usr/bin/env python
import select
import asyncore
import socket
import subprocess
import os
import os.path
import sys
import atexit
import time

controlvar = int(sys.argv[1])
host = "0.0.0.0"
port = 1337

class EchoHandler(asyncore.dispatcher_with_send):
    def handle_read(self):
        timeout=3
        total_data=[]
        data = None
        begin = time.time()
        while 1:
            if total_data and time.time()-begin > timeout:
                break
            elif time.time()-begin > timeout*2:
                break
            try:
                data = self.recv(8192)
                if data:
                    total_data.append(data)
                    begin=time.time()
                else:
                    time.sleep(3)
            except:
                pass    
                    
        count = 1
        crtl = 0
        topdir = os.getcwd()
        total_data = "".join(total_data)
        while os.path.isfile("/home/ubuntu/mapproject/results%d.txt" % count):
            count += 1
            continue
            
	datastr = open("/home/ubuntu/mapproject/results%d.txt" % count, "a+")
        datastr.write(total_data)
        datastr.write("\n")
        datastr.close()
        if os.path.getsize("%s/results%d.txt" % ("/home/ubuntu/mapproject",count)) == 0:
            subprocess.Popen(["rm", "%s/results%d.txt" % ("/home/ubuntu/mapproject",count)])
        for fil in os.listdir("/home/ubuntu/mapproject"):
            if "results" in fil and os.path.isfile(os.path.join("/home/ubuntu/mapproject",fil)):
                crtl +=1
		time.sleep(3)
        if crtl >= controlvar:
            result = []
            for entrys in os.listdir("/home/ubuntu/mapproject"):
                if "results" in entrys and os.path.isfile(os.path.join("/home/ubuntu/mapproject",entrys)):
                    result.append("%s/%s" % ("/home/ubuntu/mapproject", entrys))

            atexit.register(self.reduce, result)
            sys.exit()

    def reduce(self, dfiles):
        result_dict = {}
        for title in dfiles:
            datain = open(title, "r")
            for line in datain:
                line = line.replace("{", "")
                line = line.replace("}", "")
		try:
                    line = line.replace("\"","")
                except:
                    pass
                line = line.strip()
                line = line.split("*")
                for entry in line:
                    if line=="":
                        continue
                    else:
                        entry = entry.split(":")
			try:
                        	print entry[0], " : ", entry[1]
			except:
				continue
			
                        if entry[0].strip() in result_dict:
                            result_dict[entry[0].strip()] += int(entry[1].strip())
                        else:
                            result_dict[entry[0].strip()] = int(entry[1].strip())
            datain.close()
            
        result = open("/home/ubuntu/mapproject/finalresult.txt", "a+")
        for key,value in result_dict.items():
            print key, " : ", value
            result.write(key + " : " + str(value) + "\n")
        result.close()
            
class EchoServer(asyncore.dispatcher):
    def __init__(self, host, port):
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.set_reuse_addr()
        self.bind((host, port))
        self.listen(60)

    def handle_accept(self):
        pair = self.accept()
        if pair is not None:
            sock, addr = pair
            print 'Incoming connection from %s' % repr(addr)
            handler = EchoHandler(sock)

if __name__ == "__main__":
    server = EchoServer(host, port)
    asyncore.loop()


