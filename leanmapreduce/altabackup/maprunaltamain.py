#!/usr/bin/python

import sys
import subprocess
import os
import time
import paramiko
import random
import socket
import re
import math
import shlex

port = 22
pkey_file = "/root/.ssh/id_rsa"
localip = socket.gethostbyname(socket.gethostname())

#the remote directory for the Lean MapReduce project files on the remote host
localdir = "/home/ubuntu/mapproject"
remotedir = "/home/ubuntu/mapproject/"
transfolder = "/home/ubuntu/mapproject/transfolder"
"""
The following functions may be enabled if needed. Similar logic were used in-line:


def sftp_tx():
        transport = paramiko.Transport((host, port))
        transport.connect(username=uname, pkey=pkey)
        sftp = paramiko.SFTPClient.from_transport(transport)
        if os.path.isfile(localdir):
                sftp.put(localdir, destdir)
        elif os.path.isdir(localdir):
                for item in os.listdir(localdir):
                        filepath = os.path.join(localdir, item)
                        remotepath = os.path.join(destdir, item)
                        sftp.put(filepath,remotepath)
        return
def run_remote(host, uname, pkey, command, other, port=None, localdir=None, remotedir=None):
        key = paramiko.RSAKey.from_private_key_file(pkey)
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.load_system_host_keys()
        ssh.connect(host, username=uname, pkey=key)
        if command == None:
                print "No command was supplied"
        else:
                try:
                        stdin, stdout, stderr = ssh.exec_command(command)
                        if other != None:
                                time.sleep(2)
                                stdin.write(other)
    
                except:
                        print "please cross-check the command you attempted to execute", " \n <<<more info>>> \n\n", stderr
        if localdir!=None and destdir!=None:
                time.sleep(4)
                sftp_tx()
        ssh.close()
        return



"""


def ping(hostname):
	command = ["ping", "-c", "1", "-w", "2",  "%s" % hostname]
	runpng = subprocess.Popen(command, stdout = subprocess.PIPE)
	pngresult = runpng.communicate()[0]
        print pngresult
	pngresult = pngresult.split("\n")
	pngtime = pngresult[1].split()
	if "time" in pngtime[-2]:
		return True
	else:
		return False
		runpng.stdout.flush()
def pre_spawn(count):
	ips = 44
	for n in range(count+1):
		if n < 10:
			if n == count:
				time.sleep(5)
				break
				
                	os.chdir(localdir)
			subprocess.call(["cp", "-r", "mapresult", "mapresult00%d" % n])
			subprocess.call(["mv", "bigfile00%d" % n, "mapresult00%d/" % n])
			os.chdir("%s/mapresult00%d" % (localdir,n))
			subprocess.call(["sed", "-i", "s/10,0,0,42/10,0,0,%d/g" % ips, "service.cpp"])
			subprocess.call(["sed", "-i", "s/10.0.0.%d/10,0,0,%d/g" % (ips,ips), "service.cpp"])
			subprocess.call(["sed", "-i", "s/bigfile/bigfile00%d/g" % n, "service.cpp"])
			subprocess.call(["sed", "-i", "/^$/d", "bigfile00%d" % n])
        		subprocess.call(["sed", "-i", r"s/$/\\/g", "bigfile00%d" % n])
			subprocess.check_call(["sed", "-i", r"$ s/\\$//g", "bigfile00%d" % n])
			subprocess.call("truncate --size=-1 bigfile00%d" % n, shell=True)
#			subprocess.call(["sed", "-i", r"$s/\n$//g", "bigfile0%d" % n])
			subprocess.call(["perl", "-i", "-pe", "s/bigfile00%d/`cat bigfile00%d`/ge" % (n,n), "service.cpp"])
			subprocess.call("rm bigfile00%d" % n, shell=True)
			fnull = open(os.devnull, 'w')
			subprocess.Popen(["make"], stdout=fnull,close_fds=True)
			ips += 1
		else:
        	if n == count:
                 time.sleep(5)
                     break

             os.chdir(localdir)
             subprocess.call(["cp", "-r", "mapresult", "mapresult0%d" % n])
             subprocess.call(["mv", "bigfile0%d" % n, "mapresult0%d/" % n])
             os.chdir("%s/mapresult0%d" % (localdir,n))
             subprocess.call(["sed", "-i", "s/10,0,0,42/10,0,0,%d/g" % ips, "service.cpp"])
             subprocess.call(["sed", "-i", "s/10.0.0.%d/10,0,0,%d/g" % (ips,ips), "service.cpp"])
             subprocess.call(["sed", "-i", "s/bigfile/bigfile0%d/g" % n, "service.cpp"])
             subprocess.call(["sed", "-i", "/^$/d", "bigfile0%d" % n])
             subprocess.call(["sed", "-i", r"s/$/\\/g", "bigfile0%d" % n])
             subprocess.call(["sed", "-i", r"$ s/\\$//g", "bigfile0%d" % n])
             subprocess.call("truncate --size=-1 bigfile0%d" % n, shell=True)
             subprocess.call(["perl", "-i", "-pe", "s/bigfile0%d/`cat bigfile0%d`/ge" % (n,n), "service.cpp"])
			 subprocess.call("rm bigfile0%d" % n, shell=True)
			 fnull = open(os.devnull, 'w')
             subprocess.Popen(["make"], stdout=fnull, close_fds=True)
			 ips += 1

def start_env(count):
    pre_spawn(count)
	cpucount = 0
	ips = 44
	for m in range(count + 1):
		if m==count:
			time.sleep(8)
			break
		if m < 10:
			os.chdir("%s/mapresult00%d" % (localdir,m))
			mac = randomMAC()
			args = "/usr/bin/qemu-system-x86_64 --enable-kvm -drive file=mapresult.img,format=raw,if=ide -device virtio-net,netdev=net0,mac=%s -netdev tap,id=net0,script=/root/IncludeOS_install/etc/qemu-ifup -name includeOS%d -vga none -nographic -smp 1 -m 200" % (mac,m)
			args = shlex.split(args)

			print "starting up vm at 10.0.0.%d" % ips
			fnull=open(os.devnull, 'w')
			subprocess.Popen(["taskset", "-c", "%d" % cpucount, "/bin/bash", "OSsim.sh", "%s" % localip], stdout=fnull, close_fds=True)
			#to use includeOS uncomment the next line
			#subprocess.Popen(["taskset", "-c", "%d" % cpucount, "/bin/bash", "run.sh", "includeos_service.img"], stdout=fnull, close_fds=True)			
			print "started up vm at 10.0.0.%d" % ips
			cpucount += 1
			if cpucount == 8:
				cpucount = 0
		else:
			os.chdir("%s/mapresult0%d" % (localdir,m))
            mac = randomMAC()
            args = "/usr/bin/qemu-system-x86_64 --enable-kvm -drive file=mapresult.img,format=raw,if=ide -device virtio-net,netdev=net0,mac=%s -netdev tap,id=net0,script=/root/IncludeOS_install/etc/qemu-ifup -name includeOS%d -vga none -nographic -smp 1 -m 200" % (mac,m)
            args = shlex.split(args)

            print "starting up vm at 10.0.0.%d" % ips
            fnull=open(os.devnull, 'w')
			subprocess.Popen(["taskset", "-c", "%d" % cpucount, "/bin/bash", "OSsim.sh", "%s" % localip], stdout=fnull, close_fds=True)
			#to use includeOS uncomment the next line
			#subprocess.Popen(["taskset", "-c", "%d" % cpucount, "/bin/bash", "run.sh", "includeos_service.img"], stdout=fnull, close_fds=True)
			print "started up vm 10.0.0.%d" % ips
            cpucount += 1
			if cpucount == 8:
				cpucount = 0
		ips += 1

spawnend = None
spawnstart = None
def prepRemote(localip):
	os.chdir("/home/ubuntu/mln")
	copy = subprocess.call("cp /root/.ssh/id_rsa.pub /var/www/html/.", shell=True)
	try:
		subprocess.check_call("rm /root/.ssh/known_hosts", shell=True)
	except:
		print "no known_hosts file"
	subprocess.call(["cp", "leanmr.mln", "leanmrtemp.mln"])
	subprocess.call(["sed", "-i", "s/ipaddress/%s/g" % localip, "leanmrtemp.mln"])
	subprocess.call(["sed", "-i", "s/leanmr/leanmra2/g", "leanmrtemp.mln"])
	subprocess.call(["sed", "-i", "s/downstream1/downstreama2/g", "leanmrtemp.mln"])
	fnull = open(os.devnull, 'w')
	global spawnstart
	spawnstart = time.time()
	subprocess.call(["/usr/local/bin/mln", "build","-r","-f", "leanmrtemp.mln"],stdout=fnull, stderr=subprocess.STDOUT, close_fds=True)
	subprocess.call(["/usr/local/bin/mln", "start", "-p", "leanmra2"], stdout=fnull, stderr=subprocess.STDOUT, close_fds=True)
	status = False
	ipaddress = None
	reg_pattern = re.compile(r"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b")
	while status==False:
        	mln_stat = subprocess.Popen(["/usr/local/bin/mln", "status", "-p", "leanmra2", "-h", "downstreama2"], stdout=subprocess.PIPE)
        	entry, error = mln_stat.communicate()
        	ipaddress = reg_pattern.findall(entry)
        	for i in entry.split():
                	if "downstreama2" in i and ipaddress:
                        	ipaddress = ipaddress[0]
                        	ipaddress = ipaddress.strip()
                        	print "Address found! >> ", ipaddress
                        	status = True
                	else:
				continue
	return ipaddress



def randomMAC():
        mac = [ 0xc0, 0x01, 0x0a,
                random.randint(0x00, 0x7f),
                random.randint(0x00, 0xff),
                random.randint(0x00, 0xff) ]
        return ':'.join(map(lambda x: "%02x" % x, mac))


max_data_per_vm = None
if not sys.argv[1]:
	#Prompt the user to do so if they have not fed in the data split limit
	max_data_per_vm = int(raw_data("please enter the max size of data in megabytes for each VM: >>>   "))
else:
	#convert max data to bytes from megabytes
	max_data_per_vm = int(sys.argv[1]) * 1024 * 1024


print "max data size is >>> ", max_data_per_vm
starttime = time.time()
os.chdir(localdir)


stre = ['cat', '/proc/meminfo']
stre1 = ['awk', '/MemFree:/ { mem = $2 } /^Cached:/ { cached = $2 } END { print (mem + cached) * 1024 }']

prelim = subprocess.Popen(stre, stdout=subprocess.PIPE)
getsize = subprocess.Popen(stre1, stdin=prelim.stdout,stdout=subprocess.PIPE)
availmem = getsize.communicate()[0]
availmem = float(availmem)

""" 
Reserve 10 percent for otber events and caching needs. IncludeOS needs memory \
three times the size of the input data, hence the division by 3. This can be catered to globally.
For experiment the following formular is used to force allocatable mem size of 1100MB
"""
to_alloc = int(math.ceil((availmem - (availmem *(93.2/100)))))

#each vm should have memory twice the size of the size of its local data

max_vm_mem_size = max_data_per_vm * 3
filesize = os.path.getsize("/home/ubuntu/mapproject/bigfile2.txt")
numof_instances = None

#sentinel variable to trigger cluster growth
greater = False
#file counter of input splits to be fed to local mappers
filecnt = 0
#keep a global counter of excess input splits in case it is needed in future
excesscnt = 0

if to_alloc >= filesize:
	print "the avail mem size is >>", to_alloc
	numof_instances = int(math.ceil(float(filesize) / max_data_per_vm))
	print "we need the following number of instances", numof_instances
	subprocess.check_call(["split","-d", "--number=%d" % numof_instances, "bigfile2.txt", "bigfile0"])
else:
	print "the data size is greater than the avail mem size >>", to_alloc, filesize
	greater = True
	subprocess.call(["split","-d", "--line-bytes=%d" % to_alloc, "bigfile2.txt", "tempbig0"])
	numof_instances = int(math.ceil(float(to_alloc)/max_data_per_vm))
	filecnt = 0
	for i in os.listdir(localdir):
		if "tempbig0" in i:
			excesscnt += 1
	#excess data is combined into this a new big data file for transmission to downstream supervisor
	xdata = os.path.join(transfolder, "bigfile.txt")
	os.chdir(localdir)
	for m in range(1,excesscnt):
		if m < 10:
			subprocess.check_call("cat tempbig00%d >> %s" % (m,xdata), shell=True)
			subprocess.check_call(["rm", "tempbig00%d" % m])
		else:
			subprocess.check_call("cat tempbig0%d >> %s" % (m, xdata), shell=True)
			subprocess.check_call(["rm", "tempbig0%d" % m])
	subprocess.check_call(["split","-d", "--number=%d" % numof_instances, "tempbig000", "bigfile0"])
	subprocess.check_call(["rm", "tempbig000"])

for i in os.listdir(localdir):
        if "bigfile0" in i:
                filecnt += 1

print filecnt, " is the filecount"
if greater == True:
	hostip = prepRemote(localip)
	state = False
	while state != True:
		try:
			state = ping(hostip)
			print "Trying to reach %s " % hostip
		except:
			print "the host is unreachable"
			continue
	
	print "We have a go"
	
	#Ensure that all initial software packages have been installed, especially the webserver
	check = False
	while check==False:
		try:
			fnull = open(os.devnull, 'w')
			curl = subprocess.check_call(["curl", "-s", "%s" % hostip], stdout=fnull, close_fds=True)
			if curl == 0:
				check=True
			else:
				check=False
		except:
			check=False
			print "not ready ..."
		
	
	command ="mkdir mapproject"
	other = "yes\n"
	username = "ubuntu"
	key = paramiko.RSAKey.from_private_key_file(pkey_file)
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.load_system_host_keys()
    ssh.connect(hostip, username=username, pkey=key)
    stdin, stdout, stderr = ssh.exec_command(command)    
	try:
       	stdin.write(other)
		time.sleep(2)
		stdin.flush()
	except:
		print "not expecting input but proceeding ..."
		
	time.sleep(4)
	try:
		scp = "scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -r /home/ubuntu/mapproject/transfolder/. ubuntu@%s:/home/ubuntu/mapproject/." % hostip
		scp = shlex.split(scp)
		other ="yes\n"
		transfer = subprocess.Popen(scp, stdout=subprocess.PIPE, stdin=subprocess.PIPE)
		time.sleep(2)
		transfer.stdin.write(other)
		transfer.wait()
		transfer.stdout.flush()

	except:
		print "authentication failed: Arinze, find out what the problem is"
	
	time.sleep(20)
	data_size = int(math.ceil(float(max_data_per_vm/(1024*1024))))
	command = "sudo screen -L -d -m python /home/ubuntu/mapproject/maprunaltA.py %s %s %d" % (hostip, localip, data_size) 
	
	stdin, stdout, stderr = ssh.exec_command(command)
	time.sleep(3)
	try:
		stdin.write(other)
		stdin.flush()
	except:
		print "not expecting input but proceeding..."
	time.sleep(3)
	ssh.close()
	spawnend = time.time()
        spawntime = spawnend - spawnstart

	
	
time.sleep(5)
			
#repeat count

filecnt = 0
for i in os.listdir(localdir):
        if "bigfile0" in i:
                filecnt += 1
print "file count >> ", filecnt
#start server socket
os.chdir(localdir)        
if greater ==True:
        subprocess.Popen(["python", "server3_main.py", "%s" % str(filecnt+1)])
else:
        subprocess.Popen(["python", "server3_main.py", "%s" % str(filecnt)])


ramsize = int(math.ceil(float(max_vm_mem_size)/(1024*1024)))
#subprocess.check_call("sed -i s/128/%d/g /root/IncludeOS_install/etc/qemu_cmd.sh" % ramsize, shell=True)
		
start_env(filecnt)


check = False
while check== False:
	if os.path.isfile("/home/ubuntu/mapproject/finalresult.txt"):
		check= True
		endtime = time.time()
		interval = endtime - starttime
		timeentry = str(int(math.ceil(float(filesize/(1024**2))))).ljust(10) +  str(starttime).rjust(5) + str(endtime).rjust(15) + str(interval).rjust(15)
	        timer = open(os.path.join(localdir,"duration.txt"), "a+")
        	timer.write(timeentry + "\n")
        	timer.close()
		if greater == True:
			scp = "scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ubuntu@%s:/home/ubuntu/mapproject/spawnup /home/ubuntu/spawnup" % hostip
			scp = shlex.split(scp)
			other ="yes\n"
			transfer = subprocess.Popen(scp, stdout=subprocess.PIPE, stdin=subprocess.PIPE)
			time.sleep(5)
			try:
				transfer.stdin.write(other)
				transfer.stdout.flush()
			except:
				print "couldn't write yes"
			time.sleep(3)
			extra = open("/home/ubuntu/spawnup", "r")
			value = float(extra.read())
			print "downstream spawntime >> ", value
			extra.close()
			spawntime = value + spawntime
			entry = str(int(math.ceil(float(filesize/(1024**2))))).ljust(10) + str(2).rjust(5)  +  str(spawnstart).rjust(15) + str(spawnend).rjust(15) + str(spawntime).rjust(15)
			spawnup = open("/home/ubuntu/mapproject/spawnup", 'a+')
			spawnup.write(entry + "\n")
			spawnup.close()		













	
	
