#!/usr/bin/env bash

apt-get install -y git
apt-get install -y bridge-utils qemu-kvm qemu libvirt-bin virt-manager
sed -i /"user = \"root\""/s/^#//g /etc/libvirt/qemu.conf
sed -i /"group = \"root\""/s/^#//g /etc/libvirt/qemu.conf

/etc/init.d/libvirt-bin restart

mkdir /root/cslab/openVswitchLab/log
mkdir /root/cslab/openVswitchLab/vmimages
mkdir /root/preseed/
mv {preseed.cfg,preseed.cfg.orig} /root/preseed
git clone https://github.com/hioa-cs/IncludeOS.git
if [ -f serverinfo ];
then
        while true; do
                read -p "serverinfo exists! Do you wish to overwrite it?" yn
                case $yn in
                        [Yy]* ) echo -e "Server name \t IP address \t\t MAC address \t\t switch interface (bridge)" > serverinfo ; break;;
                        [Nn]* ) exit;;
                        * ) echo "Please answer yes or no.";;
                esac
        done
else
        echo -e "Server name \t IP address \t\t MAC address \t\t switch interface (bridge)" > serverinfo 
fi
