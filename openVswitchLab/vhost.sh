#! /bin/bash

# Using newest versions from apt
# Making desktop

if [ ! "$1" ]
then
   echo "Usage: $0 IP host number"
   exit
fi

counter=0
currBr=0
newbr=0
arraycnt=0

declare -A arr
while read line; do
        if [[ ! -z "${line// }" ]] ; then
                currBr=$line
                counter=1
        else
                counter=$counter+1
        fi
        string="${line:0:5}"
        if [[ $string == "dcsim"* || "${line// }" == "" ]] ; then

                arr[$currBr]=$((counter))
        fi
done <<-EOF
        $(brctl show | tail -n +2 | cut -f1)
EOF


if [[ " ${arr[*]} " == *"1"* ]] ; then
        for i in "${!arr[@]}"
        do
                if [[ ${arr[$i]} -lt 2 ]] ; then
                        newBr=$i
                        break
                fi
        done
else
        echo "No free switch ports. Create another vSwitch"
        exit
fi



vmNR=$1
host=192.168.122.$vmNR
user=mroot
pw=lightblue
dir=/root/cslab/openVswitchLab
ios=/root/cslab/openVswitchLab/IncludeOS
logDir=$dir/log
bridgeNum=$newBr
GB=8    # 8GB image ++
vm=server$vmNR       # VM name

virsh destroy $vm
virsh undefine $vm # Will be built from scratch

cd /root/cslab/openVswitchLab/vmimages
file=ubuntu14.04.amd64.${GB}G$vmNR.img 
/bin/rm $file
qemu-img create -f qcow2 $file ${GB}G

# Enters IP and name in /root/preseed/preseed.cfg
cat /root/preseed/preseed.cfg.orig | sed s/192.168.122.4/$host/ > /root/preseed/preseed.tmp.cfg
cat /root/preseed/preseed.tmp.cfg | sed s/192.168.122.1/192.168.122.1/ > /root/preseed/preseed1.tmp.cfg
cat /root/preseed/preseed1.tmp.cfg | sed s/control2/$vm/ > /root/preseed/preseed.cfg

date > $logDir/startVM$vmNR

/usr/bin/virt-install --name $vm --ram 1024 --vcpus=1 \
--os-type=linux  --initrd-inject=/root/preseed/preseed.cfg \
--disk path=/root/cslab/openVswitchLab/vmimages/$file,device=disk,bus=ide,format=qcow2 \
--bridge=$bridgeNum --graphics none \
--location=http://no.archive.ubuntu.com/ubuntu/dists/trusty/main/installer-amd64 \
--extra-args "auto=true file=file:/root/preseed/preseed.cfg" 

cd /root/cslab/openVswitchLab
sleep 2
echo "Waiting for virt-install to finish..."
echo ""
running=$(ps aux | grep "name $vm" | grep -v grep)

if [ ! "$running" ]
then
   echo "kvm not running, something went wrong. Exits and restarts building..."
      exec $0 "$@"
fi

# The following depends on the VM beeing booted by virt-install after building it
# If not, running=$(ps aux | grep "name $vm" | grep -v grep) should be tested for 
# and VM started after the following loop

netOK=$(ssh -o StrictHostKeyChecking=no -o BatchMode=yes root@$host pwd |& grep -e "Permission denied")
min=0
while [ ! "$netOK" ]
do
   sleep 10
   ((wait = $wait + 10))
   rest=$(($wait % 60))
   if [ $rest = 0 ]; then 
      ((min = $min + 1))
      echo -en "Has waited $min minutes\r"
      if [ $min = 60 ]; then 
         echo "Has waited 60 minutes. Exits and restarts building..."
         exec $0 "$@"
      fi
   fi
   netOK=$(ssh -o StrictHostKeyChecking=no -o BatchMode=yes root@$host pwd |& grep -e "Permission denied")
   done
echo "virt-install finished" > $logDir/finishedVM$vmNR
date >> $logDir/finishedVM$vmNR


if [ ! -f /root/.ssh/id_rsa ]; then
   ssh-keygen -t rsa -N "" -f /root/.ssh/id_rsa
fi
ssh-keygen -f "/root/.ssh/known_hosts" -R $host

empty -l
if [ $? != 0 ]; then 
   apt-get install empty-expect -y
fi

loop=0
sshOK=$(ssh -o StrictHostKeyChecking=no -o BatchMode=yes root@$host whoami |& grep -e "^root")
while [ ! "$sshOK" ]
do
   if [ $loop = 20 ]; then 
         echo "Has tried 20 times. Exits and restarts building..."
         exec $0 "$@"
   fi
   # Rebooting takes time and operation not always successfull, hence looping
   ((loop = $loop + 1))
   killall empty # Might be collision with others, but sleeptime different. Fixed in the end
   sleep $sleep
   empty -f -i in -o out scp -o StrictHostKeyChecking=no /root/.ssh/id_rsa.pub $user@$host:
   empty -w -i out -o in "assword: " "$pw\n"
   echo "Part 1 finished"
   sleep 1 
   empty -f -i in1 -o out1 ssh -o StrictHostKeyChecking=no $user@$host "mkdir ~/.ssh  2> /dev/null;cat ~/id_rsa.pub >> ~/.ssh/authorized_keys "
   empty -w -i out1 -o in1 "assword: " "$pw\n"
   echo "Part 2 finished"

   sleep 1
   empty -f -i in2 -o out2 ssh -t -o StrictHostKeyChecking=no $user@$host "sudo mkdir /root/.ssh  2> /dev/null;sudo cp /home/$user/id_rsa.pub  /root/.ssh/authorized_keys "
   empty -w -i out2 -o in2 "for $user: " "$pw\n"
   empty -s -o in2 "exit\n" 
   sleep 1
   echo "Part 3 finished"
   sshOK=$(ssh -o StrictHostKeyChecking=no -o BatchMode=yes root@$host whoami |& grep -e "^root")
done

#scp -r /root/preseed root@$host:/root/
#scp -r $dir root@$host:/root/
#scp -r $ios root@$host:/root/

ssh root@$host "echo GRUB_RECORDFAIL_TIMEOUT=2 >> /etc/default/grub"
# In order to make reboot automatic after unclean shutdown
ssh root@$host /usr/sbin/update-grub

# Installing everything using apt-get

ssh root@$host "apt-get install -y kvm arping jed"
ssh root@$host "cd $ios; ./etc/create_bridge.sh; cp ./etc/qemu-ifup /etc"
#virsh shutdown $vm
#sleep 6
#cat /etc/libvirt/qemu/$vm.xml | sed s/virbr0/$bridgeNum/ > temp.xml
#virsh shutdown $vm
#virsh destroy $vm
#virsh undefine $vm

#mv temp.xml /etc/libvirt/qemu/$vm.xml
#sleep 6
#virsh define /etc/libvirt/qemu/$vm.xml
#sleep 3
#virsh start $vm
sleep 3
if [ ! -f serverinfo ];
then
        echo -e  "Server name \t IP address \t MAC address \t switch interface (bridge)" > serverinfo
fi

for line in $(cat serverinfo);
do
        new=$(echo $line | cut -f 2)
        if [ $line == $host ];
        then
                while true; do
                        read -p "There exists a record for $host. If you select 'y', the old record will be replaced with that for the new VM if you proceed. Proceed anyway?" yn
                        case $yn in
                        [Yy]* ) sed -i "/\b$host\b/d" serverinfo; break;;
                        [Nn]* ) echo "you have chosen to retain the old record. serverinfo record is no longer up to date"; break;;
                        * ) echo "Please answer yes (y) or no (n).";;
                        esac
                done
        fi
done
sleep 3
ping -c 1 $host
sleep 2
macaddi=$(arp -a | grep $host | grep -iEo "([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})")
echo -e "$vm \t $host \t $macaddi \t $newBr" >> serverinfo


echo "IncludeOS finished" >> $logDir/finishedVM$vmNR
date >> $logDir/finishedVM$vmNR

exit



