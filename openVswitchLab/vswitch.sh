 #! /bin/bash

# Using newest versions from apt
# Making desktop

if [ ! "$1" ]
then
   echo "Usage: vmNumber: please specify 4th IP octet number of switch's IP address, e.g 155"
   exit
fi

if [ ! "$2" ]
then
   echo "Please specify number of ports as second argument"
   exit
fi

vmNR=$1
number=$2
host=192.168.122.$vmNR
gatewayip=192.168.122.1
user=mroot
pw=lightblue
dir=/root/cslab/openVswitchLab/
ios=/root/cslab/openVswitchLab/IncludeOS
logDir=$dir/log
GB=8    # 8GB image ++
vm=dcsimSwitch$vmNR       # VM name

#ifconfig virbr1 $gatewayip 
virsh destroy $vm
virsh undefine $vm # Will be built from scratch

starts=1
for (( c=$starts; c<=$number-1; c++ )); do
   if [ $(brctl show | grep -c dcsim$c) -eq 0 ]; then
      brctl addbr dcsim$c
      ifconfig dcsim$c up
   else
      continue
   fi
done

cd /root/cslab/openVswitchLab/vmimages
file=ubuntu14.04.amd64.${GB}G$vmNR.img 
/bin/rm $file

qemu-img create -f qcow2 $file ${GB}G

# Enters IP and name in /root/preseed/preseed.cfg
cat /root/preseed/preseed.cfg.orig | sed s/192.168.122.4/$host/ > /root/preseed/preseed.tmp.cfg
cat /root/preseed/preseed.tmp.cfg | sed s/192.168.122.1/$gatewayip/ > /root/preseed/preseed1.tmp.cfg
cat /root/preseed/preseed1.tmp.cfg | sed s/control2/$vm/ > /root/preseed/preseed.cfg

date > $logDir/startVM$vmNR

it=$number
brtemp="--bridge=dcsim"
bridges="--bridge=virbr0 "
for (( c=$starts; c<=$it-1; c++ )); do
    bridges+=$brtemp$c" " 
done


/usr/bin/virt-install --name $vm --ram 1024 --vcpus=1 \
--os-type=linux  --initrd-inject=/root/preseed/preseed.cfg \
--disk path=/root/cslab/openVswitchLab/vmimages/$file,device=disk,bus=ide,format=qcow2 \
$bridges\
--graphics none \
--location=http://no.archive.ubuntu.com/ubuntu/dists/trusty/main/installer-amd64 \
--extra-args "auto=true file=file:/root/preseed/preseed.cfg" 
#--extra-args "file=file:/root/preseed/preseed.cfg console=ttyS0" 

# --location=http://archive.ubuntu.com/ubuntu/dists/trusty/main/installer-amd64 \
cd /root/cslab/openVswitchLab
sleep 2
echo "Waiting for virt-install to finish..."
echo ""
running=$(ps aux | grep "name $vm" | grep -v grep)

if [ ! "$running" ]
then
   echo "kvm not running, something went wrong. Exits and restarts building..."
      exec $0 "$@"
fi

# The following depends on the VM beeing booted by virt-install after building it
# If not, running=$(ps aux | grep "name $vm" | grep -v grep) should be tested for 
# and VM started after the following loop

netOK=$(ssh -o StrictHostKeyChecking=no -o BatchMode=yes root@$host pwd |& grep -e "Permission denied")
min=0
while [ ! "$netOK" ]
do
   sleep 10
   ((wait = $wait + 10))
   rest=$(($wait % 60))
   if [ $rest = 0 ]; then 
      ((min = $min + 1))
      echo -en "Has waited $min minutes\r"
      if [ $min = 60 ]; then 
         echo "Has waited 60 minutes. Exits and restarts building..."
         exec $0 "$@"
      fi
   fi
   netOK=$(ssh -o StrictHostKeyChecking=no -o BatchMode=yes root@$host pwd |& grep -e "Permission denied")
   done
echo "virt-install finished" > $logDir/finishedVM$vmNR
date >> $logDir/finishedVM$vmNR

# Should now be possible to connect using ssh
# sleep=40
#virsh start $vm
# sleep $sleep

if [ ! -f /root/.ssh/id_rsa ]; then
   ssh-keygen -t rsa -N "" -f /root/.ssh/id_rsa
fi
ssh-keygen -f "/root/.ssh/known_hosts" -R $host

empty -l
if [ $? != 0 ]; then 
   apt-get install empty-expect -y
fi

loop=0
sshOK=$(ssh -o StrictHostKeyChecking=no -o BatchMode=yes root@$host whoami |& grep -e "^root")
while [ ! "$sshOK" ]
do
   if [ $loop = 20 ]; then 
         echo "Has tried 20 times. Exits and restarts building..."
         exec $0 "$@"
   fi
   # Rebooting takes time and operation not always successfull, hence looping
   ((loop = $loop + 1))
   killall empty # Might be collision with others, but sleeptime different. Fixed in the end
   sleep 3
   empty -f -i in -o out scp -o StrictHostKeyChecking=no /root/.ssh/id_rsa.pub $user@$host:
   empty -w -i out -o in "assword: " "$pw\n"
   echo "Part 1 finished"
   # empty -s -o in "exit\n"
   sleep 1 
   empty -f -i in1 -o out1 ssh -o StrictHostKeyChecking=no $user@$host "mkdir ~/.ssh  2> /dev/null;cat ~/id_rsa.pub >> ~/.ssh/authorized_keys "
   empty -w -i out1 -o in1 "assword: " "$pw\n"
   echo "Part 2 finished"
   #empty -s -o in2 "exit\n" 

   sleep 1
   empty -f -i in2 -o out2 ssh -t -o StrictHostKeyChecking=no $user@$host "sudo mkdir /root/.ssh  2> /dev/null; sudo cp /home/$user/id_rsa.pub  /root/.ssh/authorized_keys"
   empty -w -i out2 -o in2 "for $user: " "$pw\n"
   empty -s -o in2 "exit\n" 
   sleep 1
   echo "Part 3 finished"
   sshOK=$(ssh -o StrictHostKeyChecking=no -o BatchMode=yes root@$host whoami |& grep -e "^root")
done


ssh root@$host "echo GRUB_RECORDFAIL_TIMEOUT=2 >> /etc/default/grub"
# In order to make reboot automatic after unclean shutdown
ssh root@$host /usr/sbin/update-grub

# Installing everything using apt-get

ssh root@$host "apt-get install -y arping jed"
ssh root@$host "cd $ios; ./etc/create_bridge.sh; cp ./etc/qemu-ifup /etc"
ssh root@$host "apt-get install -y openvswitch-switch"
sleep 30

port=1
ports=$number
while [ $port -lt $ports ]; do
ssh root@$host "echo auto eth$port >> /etc/network/interfaces; echo iface eth$port inet manual >> /etc/network/interfaces; echo pre-up modprobe 8021q >> /etc/network/interfaces; echo pre-up ifconfig eth$port up >> /etc/network/interfaces;  echo post-down ifconfig eth$port down >> /etc/network/interfaces"
let port=port+1
done

port=1
while [ $port -lt $ports ]; do 
ssh root@$host "ifconfig eth$port up"
let port=port+1
done
sleep 30
ssh root@$host "ovs-vsctl add-br br0"

port=1
while [ $port -lt $ports ]; do
ssh root@$host "ovs-vsctl add-port br0 eth$port"
let port=port+1
done
ssh root@$host "ifconfig eth0 0.0.0.0;ifconfig br0 $host; ovs-vsctl add-port br0 eth0"
echo "IncludeOS finished" >> $logDir/finishedVM$vmNR
date >> $logDir/finishedVM$vmNR

exit



