
The openVswitchLab provides a framework for achieving verisimilitude to a standard network topology, through the introduction of a pseudo-switch which employs openvswitch as its forwarding fabric. This permits for traffic monitoring and control, and provides a foundation for future developments such as integration in a SDN context.

--------------------------------------------------------------------------------------------------------------------------
**THE PROJECT HAS BEEN TESTED ON UBUNTU 14.04 AND 16.04**

The default IP network for all vms created under this project is 192.168.122.0/24.

This should be performed as root, in the  /root/ directory. I.e., run  "**sudo su -**" before starting.

After cloning the project,

* For initial environment configuration,
	* change into the project directory: **cd cslab/openVswitchLab**
	* run the initialization script: **./init.sh**
* When run, vswitch.sh creates a multiport switch defined by the commandline arguments for the IP host number and desired number of ports. The build process for a new switch takes about 25 minutes
	* E.g : **./vswitch.sh 155 6** (this results in a 6 port switch with a management interface accessible at 192.168.122.155)

* To attach hosts to the switch, vhost.sh is given an IP host number. If multiple hosts are needed, only the first host needs to be built from scratch. More hosts can be provisioned as clones of the first one, orchestrated by vhostclone.sh.
	* E.g (first host) : **./vhost.sh 156**
	* E.g (clones) : **./vhostclone.sh 156 157**

When created, a vhost is dynamically attached to the vswitch on a vacant port. 

In the current state of development, a vswitch mimicks a physical switch with respect to traffic isolation. This provides the opportunity to setup a minimal security policy around inter-host network communication. 

* To learn about the IP address, mac address and associated bridge of all active servers (except switches), run the command,
	* cat serverinfo


